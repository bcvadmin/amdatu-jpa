/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.demo.client;

import java.util.List;

import org.amdatu.jpa.demo.DemoRecord;
import org.amdatu.jpa.demo.DemoService;

public class Client {

	private volatile DemoService demoService;
	
	public DemoRecord get(Long id){
		return demoService.get(id);
	}
		
	public List<DemoRecord> list(){
		return demoService.list();
	}
	
	public void add(String text){
		demoService.add(text);
	}
	
	public void delete(Long id){
		demoService.delete(id);
	}
}
