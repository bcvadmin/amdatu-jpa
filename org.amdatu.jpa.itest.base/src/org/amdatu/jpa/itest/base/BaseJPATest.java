/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.itest.base;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.amdatu.jta.manager.itest.testbundle.PropertiesBasedTestService;
import org.amdatu.jta.manager.itest.testbundle.TestEntity;
import org.amdatu.testing.configurator.TestConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class BaseJPATest {
	
    private volatile PropertiesBasedTestService instance;
    
    @Before
    public void setUp() {
 /*       configure(this)
            .add(createFactoryConfiguration("org.amdatu.jpa.datasourcefactory").set("userName", "sa").set("password", "sa")
                .set("driverClassName", "org.h2.Driver").set("jdbcUrl", "jdbc:h2:mem:test")
                .set("name", "ManagedTestDs").set("managed", "true").setSynchronousDelivery(true))
            .add(createFactoryConfiguration("org.amdatu.jpa.datasourcefactory").set("userName", "sa").set("password", "sa")
                .set("driverClassName", "org.h2.Driver").set("jdbcUrl", "jdbc:h2:mem:test")
                .set("name", "TestDs").setSynchronousDelivery(true))
             .add(createServiceDependency().setService(PropertiesBasedTestService.class).setRequired(true)).setTimeout(10, TimeUnit.SECONDS).apply();
*/    }
    
    @After
    public void tearDown(){
        TestConfigurator.cleanUp(this);
    }
    
    
    @Test
	public void testPersist() {
		long save = instance.save(new TestEntity("test"));
		assertTrue(save > 0);
		
		List<TestEntity> list = instance.list();
		assertEquals(1, list.size());
	}
	
	@Test
	public void testJPQL() {
		for(int i = 1; i <= 5; i++) {
		    instance.save(new TestEntity("test " + i));
		}
		
		List<TestEntity> list = instance.list();
		assertEquals(5, list.size());
	}
	
	//Exception happening during persist should be rethrown to the caller
	@Test
	public void testExceptionHandling() {
	    instance.save(new TestEntity("test"));
		
		try {
		    instance.save(new TestEntity("test"));
			fail("Expected unique constraint exception");
		} catch(Exception e) {
			//expected
		}
	}
	
	@Test	
	public void testFind() {
		long save = instance.save(new TestEntity("test"));
		
		assertEquals("test", instance.get(save).getName());
	}
	
	@Test
	public void testMerge() {
		long save = instance.save(new TestEntity("test"));
		
		TestEntity testEntity = instance.get(save);
		testEntity.setName("updated!");
		
		instance.update(testEntity);
		
		assertEquals("updated!", instance.get(save).getName());
	}
}
