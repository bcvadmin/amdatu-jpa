/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.util.Map;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;

/**
 * EntityManagerFactory that can't be closed manually to prevent users from stopping the EntityManagerFactory. 
 * 
 * The factory will be closed when the dependency manager component is stopped using the stop callback method
 *
 */
public class NonClosingEntityManagerFactory implements EntityManagerFactory {

    private final EntityManagerFactory m_delegate;

    public NonClosingEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        m_delegate = entityManagerFactory;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void stop() {
        m_delegate.close();
    }

    @Override
    public void close() {
        // n00p
    }

    @Override
    public EntityManager createEntityManager() {
        return m_delegate.createEntityManager();
    }

    @Override
    public EntityManager createEntityManager(@SuppressWarnings("rawtypes") Map arg0) {
        throw new RuntimeException("not supported");
    }

    @Override
    public Cache getCache() {
        return m_delegate.getCache();
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return m_delegate.getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return m_delegate.getMetamodel();
    }

    @Override
    public PersistenceUnitUtil getPersistenceUnitUtil() {
        return m_delegate.getPersistenceUnitUtil();
    }

    @Override
    public Map<String, Object> getProperties() {
        return m_delegate.getProperties();
    }

    @Override
    public boolean isOpen() {
        return m_delegate.isOpen();
    }

}
