/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.eclipselink.itest;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.util.concurrent.TimeUnit;

import org.amdatu.jpa.itest.base.BaseJPATest;
import org.amdatu.jta.manager.itest.testbundle.PropertiesBasedTestService;
import org.junit.Before;

/**
 * All test methods are part of the base class
 */
public class PostgreSQLEclipselinkTest extends BaseJPATest{
    @Before
    @Override
    public void setUp() {
    	configure(this)
		.add(createFactoryConfiguration(
				"org.amdatu.jpa.datasourcefactory")
				.set("username", "conxadmin")
				.set("password", "conxadmin")
				.set("driverClassName", "org.postgresql.Driver")
				.set("driverName", "PostgreSQL")
				.set("url",
						"jdbc:postgresql://localhost:15432/test")
				.set("name", "ManagedTestDs")
				.set("managed", "true").setSynchronousDelivery(true))
		.add(createFactoryConfiguration(
				"org.amdatu.jpa.datasourcefactory")
				.set("username", "conxadmin")
				.set("password", "conxadmin")
				.set("driverClassName", "org.postgresql.Driver")
				.set("driverName", "PostgreSQL")
				.set("url",
						"jdbc:postgresql://localhost:15432/test")
				.set("name", "TestDs")
				.set("managed", "false").setSynchronousDelivery(true))
             .add(createServiceDependency().setService(PropertiesBasedTestService.class).setRequired(true)).setTimeout(10, TimeUnit.SECONDS).apply();
    }	
}
