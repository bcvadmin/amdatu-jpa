/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.datasourcefactory;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;
import javax.transaction.TransactionManager;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.jdbc.DataSourceFactory;
import org.osgi.service.log.LogService;

public class DataSourceManagedServiceFactory implements ManagedServiceFactory {

    public static final String PID = "org.amdatu.jpa.datasourcefactory";

    private volatile LogService m_logService;

    private volatile DependencyManager m_dependencyManager;
    
    private volatile BundleContext m_bundleContext;

    private final Map<String, Component> m_components = new ConcurrentHashMap<>();

    private Properties m_defaultValidationQueries;
    
    @SuppressWarnings("unused" /* dependency manager callback */)
    private void start(){
        URL resource = m_bundleContext.getBundle().getResource("default_validation_queries");
        m_defaultValidationQueries = new Properties();
        try {
            m_defaultValidationQueries.load(resource.openStream());
        } catch (IOException e) {
            m_logService.log(LogService.LOG_WARNING, "Failed to load default validation queries", e);
        }
    } 
    
    @Override
    public String getName() {
        return PID;
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties)
        throws ConfigurationException {

        if (m_components.containsKey(pid)) {
            m_logService.log(LogService.LOG_INFO, "Removing datasource " + pid);
            Component component = m_components.remove(pid);
            m_dependencyManager.remove(component);
        }
     
        String driverClassName = getRequiredStringProperty(properties, "driverClassName");
        String validationQuery = getOptionalStringProperty(properties, "validationQuery");
        if (StringUtils.isEmpty(validationQuery)) {
            validationQuery = m_defaultValidationQueries.getProperty(driverClassName);
        }
        
        String jdbcUrl = getRequiredStringPropertyBackwardCompatible(properties, "url", "jdbcUrl");
        DataSourceBuilder dataSourceBuilder = new DataSourceBuilder()
            .setUsername(getRequiredStringPropertyBackwardCompatible(properties, "username", "userName"))   
            .setPassword(getRequiredStringProperty(properties, "password"))
            .setUrl(jdbcUrl)
            .setConnectionProperties(getOptionalStringProperty(properties, "connectionProperties"))
            .setDefaultAutoCommit(getOptionalBooleanProperty(properties, "defaultAutoCommit"))
            .setDefaultReadOnly(getOptionalBooleanProperty(properties, "defaultReadOnly"))
            .setDefaultTransactionIsolation(getOptionalIntegerProperty(properties, "defaultTransactionIsolation"))
            .setDefaultCatalog(getOptionalStringProperty(properties, "defaultCatalog"))
            .setCacheState(getOptionalBooleanProperty(properties, "cacheState"))
            .setInitialSize(getOptionalIntegerProperty(properties, "initialSize"))
            .setMaxTotal(getOptionalIntegerProperty(properties, "maxTotal"))
            .setMaxIdle(getOptionalIntegerProperty(properties, "maxIdle"))
            .setMinIdle(getOptionalIntegerProperty(properties, "minIdle"))
            .setMaxWaitMillis(getOptionalLongProperty(properties, "maxWaitMillis"))
            .setValidationQuery(validationQuery)
            .setTestOnCreate(getOptionalBooleanProperty(properties, "testOnCreate"))
            .setTestOnBorrow(getOptionalBooleanProperty(properties, "testOnBorrow"))
            .setTestOnReturn(getOptionalBooleanProperty(properties, "testOnReturn"))
            .setTestWhileIdle(getOptionalBooleanProperty(properties, "testWhileIdle"))
            .setTimeBetweenEvictionRunsMillis(getOptionalLongProperty(properties, "timeBetweenEvictionRunsMillis"))
            .setNumTestsPerEvictionRun(getOptionalIntegerProperty(properties, "numTestsPerEvictionRun"))
            .setMinEvictableIdleTimeMillis(getOptionalLongProperty(properties, "minEvictableIdleTimeMillis"))
            .setSoftMiniEvictableIdleTimeMillis(getOptionalLongProperty(properties, "softMiniEvictableIdleTimeMillis"))
            .setMaxConnLifetimeMillis(getOptionalLongProperty(properties, "maxConnLifetimeMillis"))
            .setLogExpiredConnections(getOptionalBooleanProperty(properties, "logExpiredConnections"))
            .setLifo(getOptionalBooleanProperty(properties, "lifo"))
            .setPoolPreparedStatements(getOptionalBooleanProperty(properties, "poolPreparedStatements"))
            .setMaxOpenPreparedStatements(getOptionalIntegerProperty(properties, "maxOpenPreparedStatements"));

        boolean managed = false;
        if (properties.get("managed") != null) {
            managed = BooleanUtils.toBooleanObject((String) properties.remove("managed"));
        }

        Properties dsProps = new Properties();
        
        Enumeration<String> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            
            if (Arrays.asList(new String[]{Constants.SERVICE_PID, "service.factoryPid", "felix.fileinstall.filename" }).contains(key)){
                continue;
            }
            
            Object value = properties.get(key);
            dsProps.put(key, value);

            /*
             * Compatibility with R5 and earlier releases in which the serviceName property was published as the name service property
             */
            if ("serviceName".equals(key) && properties.get("name") == null) {
                dsProps.put("name", value);
            }
        }
        
        String filter = String.format("(%s=%s)", DataSourceFactory.OSGI_JDBC_DRIVER_CLASS, driverClassName);
        Component component = m_dependencyManager
            .createComponent()
            .setInterface(DataSource.class.getName(), dsProps)
            .setImplementation(new DelegatingDataSource(dataSourceBuilder))
            .add(m_dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false))
            .add(m_dependencyManager.createServiceDependency().setService(DataSourceFactory.class, filter).setRequired(true));

        if (managed) {
            component.add(m_dependencyManager
                .createServiceDependency()
                .setService(TransactionManager.class)
                .setRequired(true));
        }

        m_logService.log(LogService.LOG_INFO, "Registering datasource [pid: " + pid + " jdbcUrl: "+jdbcUrl+"] ");

        synchronized (m_components) {
            m_dependencyManager.add(component);
            m_components.put(pid, component);
        }
    }

    private String getRequiredStringProperty(Dictionary<String, ?> properties,
        String key) throws ConfigurationException {
        String value = getOptionalStringProperty(properties, key);
        if (value == null) {
            throw new ConfigurationException(key, "Property " + key + " is required");
        }
        return value;
    }
    
    private String getOptionalStringProperty(Dictionary<String, ?> properties,
        String key) throws ConfigurationException {
        Object value = properties.remove(key);
        if (value != null) {
            return value.toString();
        }
        return null;
    }
    
    private Integer getOptionalIntegerProperty(Dictionary<String, ?> properties,
        String key) throws ConfigurationException {
        
        Object value = properties.remove(key);
        if (value != null){
            try {
                return Integer.valueOf(value.toString());
            }
            catch (NumberFormatException e){
                throw new ConfigurationException(key, String.format("Property value '%s' for propety '%s' should be an Integer value", value, key));
            }
        }
        return null; 
    }
    
    private Long getOptionalLongProperty(Dictionary<String, ?> properties,
        String key) throws ConfigurationException {
        
        Object value = properties.remove(key);
        if (value != null){
            try {
                return Long.valueOf(value.toString());
            }
            catch (NumberFormatException e){
                throw new ConfigurationException(key, String.format("Property value '%s' for propety '%s' should be a Long value", value, key));
            }
        }
        return null; 
    }
    
    private Boolean getOptionalBooleanProperty(Dictionary<String, ?> properties,
        String key) throws ConfigurationException {
        
        Object value = properties.remove(key);
        if (value != null){
            return Boolean.valueOf(value.toString());
        }
        return null; 
    }
    
    private String getRequiredStringPropertyBackwardCompatible(Dictionary<String, ?> properties,
        String key, String oldKey) throws ConfigurationException {
        String value = getOptionalStringProperty(properties, key);
        if (value == null) {
            value = getOptionalStringProperty(properties, oldKey);
            if (value == null) {
                throw new ConfigurationException(key, "Property " + key + " is required");
            }
            else {
                m_logService
                    .log(
                        LogService.LOG_WARNING,
                        String
                            .format(
                                "The property '%s' is deprecated and will be removed in a future release please use the '%s' property",
                                oldKey, key));
            }
        }
        return value;
    }
    
    @Override
    public void deleted(String pid) {
        m_logService.log(LogService.LOG_INFO, "Unregistering datasource with pid " + pid);
        synchronized (m_components) {
            if (m_components.containsKey(pid)){
                m_dependencyManager.remove(m_components.remove(pid));
            }
        }
    }

}
