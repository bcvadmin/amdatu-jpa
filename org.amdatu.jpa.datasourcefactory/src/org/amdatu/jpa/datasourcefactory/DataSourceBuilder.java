/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.datasourcefactory;

import java.sql.Driver;
import java.util.Collection;

import javax.transaction.TransactionManager;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.managed.BasicManagedDataSource;

public class DataSourceBuilder{

    private String m_username;
    private String m_password;
    private String m_url;
    private String m_connectionProperties;
    private Boolean m_defaultAutoCommit;
    private Boolean m_defaultReadOnly;
    private Integer m_defaultTransactionIsolation;
    private String m_defaultCatalog;
    private Boolean m_cacheState;
    private Integer m_initialSize;
    private Integer m_maxTotal;
    private Integer m_maxIdle;
    private Integer m_minIdle;
    private Long m_maxWaitMillis;
    private String m_validationQuery;
    private Boolean m_testOnCreate;
    private Boolean m_testOnBorrow;
    private Boolean m_testOnReturn;
    private Boolean m_testWhileIdle;
    private Long m_timeBetweenEvictionRunsMillis;
    private Integer m_numTestsPerEvictionRun;
    private Long m_minEvictableIdleTimeMillis;
    private Long m_softMiniEvictableIdleTimeMillis;
    private Long m_maxConnLifetimeMillis;
    private Boolean m_logExpiredConnections;
    private Collection<String> m_connectionInitSqls;
    private Boolean m_lifo;
    private Boolean m_poolPreparedStatements;
    private Integer m_maxOpenPreparedStatements;

    public BasicDataSource build(Driver driver, TransactionManager transactionManager) {
        
        BasicDataSource dataSource;
        if (transactionManager != null){
            @SuppressWarnings("resource")
            BasicManagedDataSource basicManagedDataSource = new BasicManagedDataSource();
            basicManagedDataSource.setTransactionManager(transactionManager);
            dataSource = basicManagedDataSource;
            
        }else{
            dataSource = new BasicDataSource();
        }
        dataSource.setDriver(driver);
        
        applyConfiguration(dataSource);

        return dataSource;
    }

    private void applyConfiguration(BasicDataSource dataSource) {
        dataSource.setUsername(m_username);
        dataSource.setPassword(m_password);
        dataSource.setUrl(m_url);

        if (m_connectionProperties != null) {
            dataSource.setConnectionProperties(m_connectionProperties);
        }
        if (m_defaultAutoCommit != null) {
            dataSource.setDefaultAutoCommit(m_defaultAutoCommit);
        }
        if (m_defaultReadOnly != null) {
            dataSource.setDefaultReadOnly(m_defaultReadOnly);
        }
        if (m_defaultTransactionIsolation != null) {
            dataSource.setDefaultTransactionIsolation(m_defaultTransactionIsolation);
        }
        if (m_defaultCatalog != null) {
            dataSource.setDefaultCatalog(m_defaultCatalog);
        }
        if (m_cacheState != null) {
            dataSource.setCacheState(m_cacheState);
        }
        if (m_initialSize != null) {
            dataSource.setInitialSize(m_initialSize);
        }
        if (m_maxTotal != null) {
            dataSource.setMaxTotal(m_maxTotal);
        }
        if (m_maxIdle != null) {
            dataSource.setMaxIdle(m_maxIdle);
        }
        if (m_minIdle != null) {
            dataSource.setMinIdle(m_minIdle);
        }
        if (m_maxWaitMillis != null) {
            dataSource.setMaxWaitMillis(m_maxWaitMillis);
        }
        if (m_validationQuery != null) {
            dataSource.setValidationQuery(m_validationQuery);
        }
        if (m_testOnCreate != null) {
            dataSource.setTestOnCreate(m_testOnCreate);
        }
        if (m_testOnBorrow != null) {
            dataSource.setTestOnBorrow(m_testOnBorrow);
        }
        if (m_testOnReturn != null) {
            dataSource.setTestOnReturn(m_testOnReturn);
        }
        if (m_testWhileIdle != null) {
            dataSource.setTestWhileIdle(m_testWhileIdle);
        }
        if (m_timeBetweenEvictionRunsMillis != null) {
            dataSource.setTimeBetweenEvictionRunsMillis(m_timeBetweenEvictionRunsMillis);
        }
        if (m_numTestsPerEvictionRun != null) {
            dataSource.setNumTestsPerEvictionRun(m_numTestsPerEvictionRun);
        }
        if (m_minEvictableIdleTimeMillis != null) {
            dataSource.setMinEvictableIdleTimeMillis(m_minEvictableIdleTimeMillis);
        }
        if (m_softMiniEvictableIdleTimeMillis != null) {
            dataSource.setSoftMinEvictableIdleTimeMillis(m_softMiniEvictableIdleTimeMillis);
        }
        if (m_maxConnLifetimeMillis != null) {
            dataSource.setMaxConnLifetimeMillis(m_maxConnLifetimeMillis);
        }
        if (m_logExpiredConnections != null) {
            dataSource.setLogExpiredConnections(m_logExpiredConnections);
        }
        if (m_connectionInitSqls != null) {
            dataSource.setConnectionInitSqls(m_connectionInitSqls);
        }
        if (m_lifo != null) {
            dataSource.setLifo(m_lifo);
        }
        if (m_poolPreparedStatements != null) {
            dataSource.setPoolPreparedStatements(m_poolPreparedStatements);
        }
        if (m_maxOpenPreparedStatements != null) {
            dataSource.setMaxOpenPreparedStatements(m_maxOpenPreparedStatements);
        }

    }

    public DataSourceBuilder setUsername(String username) {
        m_username = username;
        return this;
    }

    public DataSourceBuilder setPassword(String password) {
        m_password = password;
        return this;
    }

    public DataSourceBuilder setUrl(String url) {
        m_url = url;
        return this;
    }

    public DataSourceBuilder setConnectionProperties(String connectionProperties) {
        m_connectionProperties = connectionProperties;
        return this;
    }

    public DataSourceBuilder setDefaultAutoCommit(Boolean defaultAutoCommit) {
        m_defaultAutoCommit = defaultAutoCommit;
        return this;
    }

    public DataSourceBuilder setDefaultReadOnly(Boolean defaultReadOnly) {
        m_defaultReadOnly = defaultReadOnly;
        return this;
    }

    public DataSourceBuilder setDefaultTransactionIsolation(Integer defaultTransactionIsolation) {
        m_defaultTransactionIsolation = defaultTransactionIsolation;
        return this;
    }

    public DataSourceBuilder setDefaultCatalog(String defaultCatalog) {
        m_defaultCatalog = defaultCatalog;
        return this;
    }

    public DataSourceBuilder setCacheState(Boolean cacheState) {
        m_cacheState = cacheState;
        return this;
    }

    public DataSourceBuilder setInitialSize(Integer initialSize) {
        m_initialSize = initialSize;
        return this;
    }

    public DataSourceBuilder setMaxTotal(Integer maxTotal) {
        m_maxTotal = maxTotal;
        return this;
    }

    public DataSourceBuilder setMaxIdle(Integer maxIdle) {
        m_maxIdle = maxIdle;
        return this;
    }

    public DataSourceBuilder setMinIdle(Integer minIdle) {
        m_minIdle = minIdle;
        return this;
    }

    public DataSourceBuilder setMaxWaitMillis(Long maxWaitMillis) {
        m_maxWaitMillis = maxWaitMillis;
        return this;
    }

    public DataSourceBuilder setValidationQuery(String validationQuery) {
        m_validationQuery = validationQuery;
        return this;
    }

    public DataSourceBuilder setTestOnCreate(Boolean testOnCreate) {
        m_testOnCreate = testOnCreate;
        return this;
    }

    public DataSourceBuilder setTestOnBorrow(Boolean testOnBorrow) {
        m_testOnBorrow = testOnBorrow;
        return this;
    }

    public DataSourceBuilder setTestOnReturn(Boolean testOnReturn) {
        m_testOnReturn = testOnReturn;
        return this;
    }

    public DataSourceBuilder setTestWhileIdle(Boolean testWhileIdle) {
        m_testWhileIdle = testWhileIdle;
        return this;
    }

    public DataSourceBuilder setTimeBetweenEvictionRunsMillis(Long timeBetweenEvictionRunsMillis) {
        m_timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
        return this;
    }

    public DataSourceBuilder setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun) {
        m_numTestsPerEvictionRun = numTestsPerEvictionRun;
        return this;
    }

    public DataSourceBuilder setMinEvictableIdleTimeMillis(Long minEvictableIdleTimeMillis) {
        m_minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
        return this;
    }

    public DataSourceBuilder setSoftMiniEvictableIdleTimeMillis(Long softMiniEvictableIdleTimeMillis) {
        m_softMiniEvictableIdleTimeMillis = softMiniEvictableIdleTimeMillis;
        return this;
    }

    public DataSourceBuilder setMaxConnLifetimeMillis(Long maxConnLifetimeMillis) {
        m_maxConnLifetimeMillis = maxConnLifetimeMillis;
        return this;
    }

    public DataSourceBuilder setLogExpiredConnections(Boolean logExpiredConnections) {
        m_logExpiredConnections = logExpiredConnections;
        return this;
    }

    public DataSourceBuilder setConnectionInitSqls(Collection<String> connectionInitSqls) {
        m_connectionInitSqls = connectionInitSqls;
        return this;
    }

    public DataSourceBuilder setLifo(Boolean lifo) {
        m_lifo = lifo;
        return this;
    }

    public DataSourceBuilder setPoolPreparedStatements(Boolean poolPreparedStatements) {
        m_poolPreparedStatements = poolPreparedStatements;
        return this;
    }

    public DataSourceBuilder setMaxOpenPreparedStatements(Integer maxOpenPreparedStatements) {
        m_maxOpenPreparedStatements = maxOpenPreparedStatements;
        return this;
    }

}
